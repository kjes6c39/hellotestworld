//
//  SceneDelegate.h
//  HelloTestWorld
//
//  Created by Ind-QA-mac-pro on 2019/11/13.
//  Copyright © 2019 Ind-QA-mac-pro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

