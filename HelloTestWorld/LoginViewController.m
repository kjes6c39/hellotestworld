//
//  LoginViewController.m
//  HelloTestWorld
//
//  Created by Ind-QA-mac-pro on 2020/1/8.
//  Copyright © 2020 Ind-QA-mac-pro. All rights reserved.
//

#import "LoginViewController.h"
#import "ViewController.h"

@interface LoginViewController ()

@property(nonatomic,strong) UITextField *nameTextField;
@property(nonatomic,strong) UITextField *passWordTextField;
@property(nonatomic,strong) UIImageView *dogImageView;
@property(nonatomic,strong) UIButton *loginBtn;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    CGSize screenSize = UIScreen.mainScreen.bounds.size;
    self.view.accessibilityLabel = @"view";

    float x, y, w, h;
    x = y = 0;
    w = screenSize.width;
    h = screenSize.height;
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    bgImageView.frame = CGRectMake(x, y, w, h);
    bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    bgImageView.alpha = 0.6;
    [self.view addSubview:bgImageView];
    
    w = h = 100;
    x = screenSize.width / 2 - w / 2;
    y = 200;
    self.dogImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    self.dogImageView.image = [UIImage imageNamed:@"cat"];
    self.dogImageView.alpha = 0.8;
    self.dogImageView.userInteractionEnabled = NO;
    [self.view addSubview:self.dogImageView];
    
    UIButton *dogBtn = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
    [dogBtn addTarget:self action:@selector(shakeDog) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:dogBtn];

    w = 270;
    h = 50;
    x = screenSize.width / 2 - w / 2;
    y = 200;
    self.nameTextField = [[UITextField alloc]initWithFrame:CGRectMake(x, y, w, h)];
    self.nameTextField.alpha = 0.7;
    self.nameTextField.layer.cornerRadius = 10;
    self.nameTextField.layer.masksToBounds = YES;
    self.nameTextField.accessibilityLabel = @"nameTextField";
    self.nameTextField.backgroundColor = [UIColor whiteColor];
    self.nameTextField.textColor = [UIColor blackColor];
    self.nameTextField.attributedPlaceholder = [[NSAttributedString alloc]
        initWithString:@"請輸入帳號" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    [self.view addSubview:self.nameTextField];
    
    y = CGRectGetMaxY(self.nameTextField.frame) + 20;
    self.passWordTextField = [[UITextField alloc]initWithFrame:CGRectMake(x, y, w, h)];
    self.passWordTextField.alpha = 0.7;
    self.passWordTextField.layer.cornerRadius = 10;
    self.passWordTextField.layer.masksToBounds = YES;
    self.passWordTextField.accessibilityLabel = @"passWordTextField";
    self.passWordTextField.backgroundColor = [UIColor whiteColor];
    self.passWordTextField.secureTextEntry = YES;
    self.passWordTextField.textColor = [UIColor blackColor];
    self.passWordTextField.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:@"請輸入密碼" attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    [self.view addSubview:self.passWordTextField];

    y = CGRectGetMaxY(self.passWordTextField.frame) + 60;
    self.loginBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, y, w, h)];
    self.loginBtn.backgroundColor = [UIColor blackColor];
    self.loginBtn.alpha = 0.7;
    self.loginBtn.layer.cornerRadius = 10;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.accessibilityLabel = @"loginBtn";
    [self.loginBtn setTitle:@"登入" forState:UIControlStateNormal];
    [self.loginBtn addTarget:self action:@selector(loginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:self.loginBtn];
    
}

- (void)loginBtnClick {
    
    NSString *name = self.nameTextField.text;
    NSString *password = self.passWordTextField.text;
    
    [self shakeToShow:self.loginBtn];
    
    if ([name isEqualToString:@""] || [password isEqualToString:@""]) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"帳號或密碼不得為空" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [controller addAction:okAction];
        [self presentViewController:controller animated:YES completion:nil];
        return;
    }

    if ([[name lowercaseString] isEqualToString:@"qa"] && [password isEqualToString:@"123"]) {
        
        [self prsentToVC];
        
    } else {
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"帳號或密碼輸入錯誤" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        okAction.accessibilityLabel = @"確認";
        [controller addAction:okAction];
        [self presentViewController:controller animated:YES completion:nil];
    }
    
    [self clearTextField];
}

- (void)prsentToVC {
    ViewController *vc = [[ViewController alloc]init];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)clearTextField {
    self.nameTextField.text = @"";
    self.passWordTextField.text = @"";
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)shakeToShow:(UIButton *)button{

    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];

    animation.duration = 0.5;

    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [button.layer addAnimation:animation forKey:nil];
}

-(void)shakeDog{

    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.1;
    animation.repeatCount = 4;
    animation.autoreverses = YES;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    
    animation.values = values;
    [self.dogImageView.layer addAnimation:animation forKey:nil];
}

@end
