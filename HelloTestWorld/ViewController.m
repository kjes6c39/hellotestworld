//
//  ViewController.m
//  HelloTestWorld
//
//  Created by Ind-QA-mac-pro on 2019/11/13.
//  Copyright © 2019 Ind-QA-mac-pro. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(nonatomic,strong) UIView *infoView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    CGSize screenSize = UIScreen.mainScreen.bounds.size;
    
    UIButton *buttonOne = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height / 3)];
    buttonOne.backgroundColor = [UIColor colorWithRed:1 green: 0.4932718873 blue:0.4739984274 alpha:1];
    buttonOne.accessibilityLabel = @"buttonOne";
    [buttonOne setTitle:@"出現提示框" forState:UIControlStateNormal];
    [buttonOne setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buttonOne addTarget:self action:@selector(btnOneClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonOne];
    
    UIButton *buttonTwo = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(buttonOne.frame), screenSize.width, screenSize.height / 3)];
    buttonTwo.backgroundColor = [UIColor colorWithRed:0.8549019694 green:0.250980407 blue:0.4784313738 alpha:1];
    buttonTwo.accessibilityLabel = @"buttonTwo";
    [buttonTwo setTitle:@"出現選單" forState:UIControlStateNormal];
    [buttonTwo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buttonTwo addTarget:self action:@selector(btnTwoClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonTwo];
    
    UIButton *buttonThree = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(buttonTwo.frame), screenSize.width, screenSize.height / 3)];
    buttonThree.backgroundColor = [UIColor colorWithRed:0.8078431487 green:0.02745098062 blue:0.3333333433 alpha:1];
    buttonThree.accessibilityLabel = @"buttonThree";
    [buttonThree setTitle:@"提示畫面" forState:UIControlStateNormal];
    [buttonThree setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [buttonThree addTarget:self action:@selector(btnThreeClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonThree];
    
    self.infoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    self.infoView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    self.infoView.alpha = 0;
    
    UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    infoLabel.text = @"網路異常請稍等";
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    [self.infoView addSubview:infoLabel];
    [self.view addSubview:self.infoView];
    
}

- (void)btnOneClick {

    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"你按了提示框" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    okAction.accessibilityLabel = @"okAction";
    [controller addAction:okAction];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)btnTwoClick {

    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"中午想吃啥?" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSArray *arr = @[@"便當", @"拉麵", @"蛋餅"];
    
    for (NSString *str in arr) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:str style:UIAlertActionStyleDefault handler:nil];
        [controller addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"都不要" style:UIAlertActionStyleCancel handler:nil];
    cancelAction.accessibilityLabel = @"cancelAction";
    [controller addAction:cancelAction];
    
    [self presentViewController:controller animated:YES completion:nil];
    
}

- (void)btnThreeClick {

    [UIView animateWithDuration:2 animations:^{
        self.infoView.alpha = 1;
    } completion:^(BOOL finished) {
        self.infoView.alpha = 0;
    }];
}

@end
